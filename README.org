#+title: Org Mode
#+date: <2021-07-03 10:52>
#+description: org-mode
#+filetags: org-mode emacs spacemacs org org-babel literate-programming programming
#+PROPERTY: header-args :results output

* Literate Programming with org-babel

  [[./literate-programming-org-babel.org]]


* Get into organization mode! :noexport:

  [[./GetIntoOrganizationMode.org]]

* scratch :noexport:
#+name: foo
#+begin_src ruby :exports both :var n=0
  puts "#{n}-foobar"
#+end_src

#+RESULTS: foo
: 0-foobar

src_ruby[:exports both :var n=0]{puts "#{n}-foobar"} {{{results(=0-foobar=)}}}

#+CALL: foo(n=1) :exports results

#+RESULTS:
: 1-foobar
